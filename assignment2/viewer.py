'''
viewer.py
ytanay/cyber-assignments/assignment2
Part of a series in... cyber

A viewer for PCAP files. You did not see that coming.

Written by Yotam Tanay, 2014
'''

import sys, os.path, time, operator
from scapy.all import *

class Mapper(object):
  '''This static class does mapping, from ICMP/IPv4 codes to something more... human. It also does some pretty printing for packets.'''

  @staticmethod
  def type(code):
    '''Like the above states, this maps ICMP codes to a textual representation of the packet (thanks, Wikipedia!)'''

    try:
      return [
        'Ping Reply', #0
        'Reserved', #1
        'Reserved', #2
        'Destination Unreachable', #3
        'source Quench', #4
        'Redirect Message', #5
        'Alternate Host Address', #6
        'Reserved', #7
        'Ping Request', #8
        'Router Advertisement', #9
        'Router Solicitation', #10
        'Time Exceeded', #11
        'Bad IP header', #12
        'Timestamp', #13
        'Timestamp Reply', #14
        'Information Request', #15
        'Information Reply', #16
        'Address Mask Request', #17
        'Address Mask Reply', #18
        'Reserved for security' #19
      ][code]

    except Exception, e:
      if code == 30:
        return 'Traceroute Request'
      else:
        return 'Unknown/Common'
    
  @staticmethod
  def packet(data):
    '''Pretty prints ICMP packets'''
    
    bumper = ' ' * 6

    info = '%sSource address: %s, IP: %s\n' % (bumper, data[0].src, data[1].src)
    info += '%sDestination address: %s, IP: %s\n' % (bumper, data[0].dst, data[1].dst)
    info += '%sFlags: %s, Length: %d, TTL: %d, Checksum %d\n' % (bumper, data[1].flags, data[1].len, data[1].ttl, data[1].chksum)
    info += '%sType: %s (%s), Code %d, Seq: %s' % (bumper, data[2].type, Mapper.type(data[2].type), data[2].code, data[2].seq)
    
    return info

    '''I'm well aware that this should be padded instead of bumped. However, since Windows is horrible at this stuff, and often decides to skip lines, I'd rather take the safe route'''
    
  @staticmethod
  def overview(packet):
    '''Creates a short overview of a packet for the table'''

    return '%s, (%d, C%d)' % (Mapper.type(packet.type), packet.id, packet.code)


class Viewer(object):
  '''The PCAP viewer wrapper, for... PCAP files'''
  
  def __init__(self, path):
    super(Viewer, self).__init__()
    
    self.load(path) # tries to load the capture file
    self.filter(1) # filters ICMP packets

    print 'Successfully read file %s. File contains %d packets in total.' % (path, len(self.packets))
    print 'Filtered all ICMP packets. There are %d packets in total' % len(self.filtered)

    print '\nReady for commands - here is the guide:'

    self.explain() # shows the guide
    self.prompt() # prompts user input

  def load(self, path):
    '''Now if this isn't the most interactively annoying file reading mechanism, I don't know what is'''

    while True:
      if not os.path.isfile(path):
        print 'The file %s was not found on this system. Please enter a file path, again:' % path
        path = raw_input()
      else:
        try:
          self.path = path
          self.packets = rdpcap(path)
          break
        except Exception, e:
          print 'The file %s is not a valid capture file. Seriously, please give me a capture file:' % path
          path = raw_input()

    '''By the way, this anti-pattern is done at Raia's insistence. Ask her if you don't believe me.'''

  def filter(self, protocol):
    '''Filters packet by a protocol (hint: ICMP is 1)'''

    self.filtered = self.packets.filter(lambda (packet): hasattr(packet, 'proto') and packet.proto == protocol)

  def table(self):
    '''
      Prints a stunningly pretty table with very little info, and yes, that is on purpose (purpose: saving characters in the tiny console window).
      If you want detailed packet inspection, use the `associate` command or, oh, I don't know, Wireshark?
    '''

    print 'ICMP packets table - rows: sources, columns: destinations. The intersection is the packet type.\n'
    print '=' * 80
    self.filtered.make_table(lambda (packet): ('Src: %s' % packet[1].src, 'Dest: %s' % packet[1].dst, Mapper.overview(packet[2]))) # This is why I don't like Scapy
    print '=' * 80
    print 'For more detailed info, please use the `associate` command'


  def associate(self):
    '''This is the cool bit which associates requests and responses! With dictionaries!'''
   
    print 'ICMP associations - each request/response has an ID. I\'ll iterate over them and show you data for each packet'
    print '=' * 80
    associations = {} # We'll store our associations here

    # First, we look at all our packets
    for packet in self.filtered:
      icmp = packet[2] # Grab the ICMP data from the Scapy composite
      id = str(icmp.id) # And gives it a "stringy" id

      # Now we'll see if we already have an entity for this id. If not, we'll create it.
      if not id in associations:
        associations[id] = {}

      associations[id][Mapper.type(icmp.type)] = packet # Finally, we'll push a reference to this packet into our entity

    counter = 0; # Because it's prettier that way

    # Now that we grouped everything together, we can print it. We'll go over each association
    for id, assoc in associations.iteritems():
      counter += 1
      print '  %d) Inspecting transfer %s' % (counter, id)

      # Just to be on the safe side, we'll sort the entities in this transfer in reverse, so that requests will go before replies.
      sort = sorted(assoc.iteritems(), key=operator.itemgetter(0), reverse=True)
      
      # And now we just need to print our packets!
      for entity in sort:
        print '%sPacket: %s:' % (' ' * 4, entity[0])
        print Mapper.packet(entity[1])

    print '=' * 80

  def prompt(self):
    '''Prompts the user for a command'''
    
    # Observer, the anti-pattern!
    while True:

      time.sleep(1.5) # This is to make the interface less jumpy. We wait for a second and a half before displaying the prompt, else user might not notice the command has been executed, and he/she will be confued. Oh, the poor, poor, user. 

      print '\nWhat would you like to do now? [table, associate/assoc, load, interact, help, exit]'
      
      selection = raw_input()

      if selection == 'table': # shows the table
        self.table()

      elif selection == 'associate' or selection == 'assoc': # shows associations
        self.associate()

      elif selection == 'load': # loads a new file
        print 'Sure thing, just give me a file name!'
        self.load(raw_input())
        self.filter(1)
        print 'All done! New file loaded.'

      elif selection == 'interact': # drop into REPL - useful for introspection.
        print 'Dropping out of prompt loop. Hopefuly you used `python -i`'
        break

      elif selection == 'help': # prints... the help.
        print 'Here you go:'
        self.explain()

      elif selection == 'close' or selection == 'exit': #exits!
          print 'This was fun! Let\'s do it again some time - goodbye for now.'
          sys.exit(0)

      else: # oh my!
        self.invalid(selection) 

  def explain(self):
    '''This prints a short list of the commands and their description'''

    print '\ttable - lists all ICMP packets in a table format. Rows represent source addresses, and columns represent destinations.'
    print '\tassociate/assoc - associates ICMP requests and responses in a convenient view. For example; ping request and replies.'
    print '\tload - Loads a new capture file.'
    print '\tinteract - exits the prompt to allow for introspection. You must use this with `python -i`'
    print '\thelp - show this help (surprise!)'
    print '\texit - brews fresh coffe from properly ground beans. Please specify milk ratio to your preference.'

  def invalid(self, command):
    '''Runs when a user enters an unknown command'''

    print 'Sorry, I don\'t know what you mean by %s. Try one of these commands: [table, associate, load, interact, help, exit]' % command
    print 'In the meantime, here is the help:'
    self.explain()


'''Now for __main__!'''
    
print '\nAssignment #2 - An ICMP viewer for pcap. Written by Yotam Tanay, 2014'
print 'Usage: python %s [filename]\n' % sys.argv[0]

try:
  path = str(sys.argv[1])
except IndexError:
  print 'This program requires a capture file to run. No file was entered as an argument. Please enter a file path:' % path
  path = raw_input()

viewer = Viewer(path)
