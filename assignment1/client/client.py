"""
client.py
ytanay/cyber-assignments/assignment1
Part of a series in... "cyber"

Handles messages in and out of the socket for the GUI (interface.py), with a nice request/response model.
It's kind of a test to see whether the beautiful callback-based model everyone loves can be forced onto Python.

Spoiler: not really.

Written by Yotam Tanay, 2014
"""

import socket, select, string, sys

class ClientHandler(object):
	"""
                This is the wrapper for the client tasks. It pushes requests on the socket and constructs response objects, when it gets them.
		It uses a simple callback mechanic (which the smart people from Node.js like).
	"""
	def __init__(self, host, port, callback):
		super(ClientHandler, self).__init__()
		self.host = host;
		self.port = port
		self.callback = callback
		self.socket = self.connect(host, port)

	def connect(self, host, port):
		"""Try to connect to the server and construct a socket"""
		newSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		newSocket.settimeout(2) #I've been toold this is a good middle ground
		newSocket.connect((host, port))
		return newSocket

	def awaitResponse(self):
		'''
                  Horribly, waits for messages from the server. This SHOULD be async, and pulled off to a seperate thread.
		  However, I've been informed that usage of advanced Python mechanics is not allowed.
		  Go figure.
		'''
		while 1:

			# Oh man... this is so C-ish. I love it! It's almost as if Python got something right.
			read_sockets, write_sockets, error_sockets = select.select([self.socket] , [], [])
			 
			for sock in read_sockets:

				# See if we got a response from the server
				if sock == self.socket:
					data = sock.recv(4096)

					if not data:
						self.callback('DISCONNECT')
						self.socket.close()
					else:
						self.callback(data)
					return
					
	def send(self, command, args):
		"""Sends a command over the socket"""
		self.socket.send(command + ' ' + args)
		

# Sorry, had to do this.
if __name__ == '__main__':
	import Tkinter, tkMessageBox 
	root = Tkinter.Tk()
	root.withdraw()
	tkMessageBox.showerror("I caught you!", "So, you didn't read the README, have you? You're supposed to use interface.py...\n\nTsk Tsk...")
