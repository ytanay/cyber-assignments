"""
interface.py
ytanay/cyber-assignments/assignment1
Part of a series in... "cyber"

The GUI interface, written with Tkinter.

Written by Yotam Tanay, 2014
"""

import Tkinter, tkMessageBox, json

from client import ClientHandler

class Interface(object):
  """The GUI wrapper. Tk... well, I'd rather not talk about it."""

  def __init__(self):
    super(Interface, self).__init__()
    print 'interface.py - ytanay/cyber-assignments/assignment1 - written by Yotam Tanay, 2014'
    self.currentView = None
    self.ready = False
    self.build()
    self.view('connect')
    
    self.root.mainloop()

  def build(self):
    """This initializes Tkinter and builds the weirdly dysfunctional view system"""

    root = Tkinter.Tk()
    self.builder = self.Builder()

    self.builder.top(root)

    self.views = {
      'connect': self.builder.connect(root, self.connect),
      'main': self.builder.main(root, self.send)
    }

    self.builder.textStatus.tag_config("warning", background="yellow", foreground="blue")
    self.builder.textStatus.tag_config("error", background="red", foreground="white")

    self.root = root

  def connect(self):
    """The callback from the connect button"""

    print("Interface::conect(): attempting connection")
    try:
      self.client = ClientHandler((self.builder.fieldHost.get() or "localhost"), int(self.builder.fieldPort.get() or 8888), self.callback)
      self.client.awaitResponse()
    except Exception:
      self.fail()
      return

  def send(self):
    """Sends a command to the server"""
    selected = self.builder.listCommands.curselection()
    name = self.builder.listCommands.get(selected)
    
    if '*' in name:
      name = name[:-2]
      if not tkMessageBox.askyesno('Long running process.', 'Warning! This process (`%s`), might take a few seconds to run, and the client will not respond until the output has been buffered.This DOES NOT mean it has crashed!\nWould you like to continue?' % name):
        return

    self.builder.textStatus.insert('end', 'SEND EXEC ' + name + '\n', ('warning'))
    self.builder.textResponse.delete('1.0', 'end')
      
    try:
      self.client.send('EXEC', name)
      self.client.awaitResponse()
    except Exception:
      self.fail()
      return


  def view(self, name):
    """Changes the current view. Oh yes. It's horrible. But that's part of the fun, you know."""

    if self.currentView == name:
      print('Interface::view(): skipping update')
      return False

    self.currentView = name

    print('Interface::view(): changing view to %s' % name)

    #First we unpack old elements
    for key, view in self.views.iteritems():
      for element in view[1]:
          element.pack_forget()

    
    #Now we pack in special elements.
    self.builder.package(name)

    #Finally, we pack everything else.
    if self.views[name][0]:
      for element in self.views[name][1]:
        element.pack()

    return True

  def callback(self, data):
    """This gets fired when we get a message back from the server"""
    
    print 'interface::update*callback: recieved data "%s"...' % data[:45]

    responses = data.split('\n')
    done = True
    
    for line in responses:
      if line.startswith('DISCONNECT'):
        tkMessageBox.showerror("Disconnected", "Disconnected from the server. I bet this is your fault.")
        print("Disconnected!")
      elif line.startswith('ERROR'):
        self.builder.textStatus.insert('end', line, ('error'))
      elif line.startswith('WRITE'):
        self.builder.textResponse.insert(Tkinter.END, line[6:] + '\n')
        done = False
      elif line.startswith('DONE'):
        done = True
      elif line.startswith("UPDATE_COMMANDS"):
        self.ready = True
        spec = line.split(' -> ')
        self.builder.listCommands.delete(0, Tkinter.END)
        self.builder.listCommands.insert(Tkinter.END, *spec[1].split(','))
        self.view('main') #switch to the main view when we get our commands
      elif len(line.split()) > 0:
        self.builder.textStatus.insert(Tkinter.END, line + '\n')

    if not done or not self.ready:
      self.client.awaitResponse()

  class Builder(object):
    """
      Build the interface
      I actually think this is a pretty nifty trick. (Tk is a really horrid way to build interfaces)
    """
    def __init__(self):
      pass
    
    def top(self, root):
      """Builds the static top fixture of the interface"""

      labelframe = Tkinter.LabelFrame(root, text="Assignment 1, C.S. Cyber Course, De-Shalit H.S.")
      labelframe.pack(fill="both", expand="yes")
      
      self.labelTop1 = Tkinter.Label(labelframe, text='Multithreaded server/client communication over TCP w/ remote command execution. Uses Tkinter and supports multiple clients.')
      self.labelTop2 = Tkinter.Label(labelframe, text='Eran, please see README for details. Written by Yotam Tanay, 05/09/14. Some commands take a long to time to run - blame Win32.')

      self.labelTop1.pack()
      self.labelTop2.pack()
      #self.labelTop3.pack()

    def connect(self, root, connectCallback):
      """Builds the connection interface"""

      self.labelHelp = Tkinter.Label(root, text="Welcome! Please connect to the server with a host and port!")
      self.labelHost = Tkinter.Label(root, text="Hostname:")
      self.labelPort = Tkinter.Label(root, text="Port:")
      
      self.fieldHost = Tkinter.Entry(root)
      self.fieldPort = Tkinter.Entry(root)

      self.buttonConnect = Tkinter.Button(root, text="Connect", command=connectCallback)

      self.fieldHost.insert(0, "localhost")
      self.fieldPort.insert(0, "8888")

      return (True, [self.labelHelp, self.labelHost, self.fieldHost, self.labelPort, self.fieldPort, self.buttonConnect]) # The first argument of the tuple states wether this view can be automatically built

    def main(self, root, sendCallback):
      """Builds the main viewport of the interface"""
      
      self.labelInstruction = Tkinter.Label(root, text='Commnds:')
      
      self.frameCommands = Tkinter.Frame(root)
      self.scrollCommands = Tkinter.Scrollbar(self.frameCommands)
      self.listCommands = Tkinter.Listbox(self.frameCommands, yscrollcommand=self.scrollCommands.set)
      self.scrollCommands.config(command=self.listCommands.yview)

      self.buttonSend = Tkinter.Button(root, text="Send!", command=sendCallback)

      self.labelResponse = Tkinter.Label(root, text='Response:')

      self.frameResponse = Tkinter.Frame(root)
      self.scrollResponse = Tkinter.Scrollbar(self.frameResponse)
      self.textResponse = Tkinter.Text(self.frameResponse, yscrollcommand=self.scrollResponse.set)
      self.scrollResponse.config(command=self.textResponse.yview)

      self.labelStatus = Tkinter.Label(root, text='Status:')

      self.frameStatus = Tkinter.Frame(root)
      self.scrollStatus = Tkinter.Scrollbar(self.frameStatus)
      self.textStatus = Tkinter.Text(self.frameStatus, yscrollcommand=self.scrollStatus.set)
      self.scrollStatus.config(command=self.textStatus.yview)

      return (False, [self.labelInstruction, self.frameCommands, self.scrollCommands, self.listCommands, self.buttonSend, self.labelResponse, self.frameResponse, self.scrollResponse, self.textResponse, self.labelStatus, self.frameStatus, self.scrollStatus, self.textStatus])

    def package(self, name):
      """Packs special components which require extra configuration"""

      if name == "main":
        self.labelInstruction.pack()

        self.frameCommands.pack()
        self.scrollCommands.pack(side=Tkinter.RIGHT, fill=Tkinter.Y)
        self.listCommands.pack()

        self.buttonSend.pack()

        self.labelResponse.pack()

        self.frameResponse.pack()
        self.scrollResponse.pack(side=Tkinter.RIGHT, fill=Tkinter.Y)
        self.textResponse.pack()
       
        self.labelStatus.pack()
        
        self.frameStatus.pack()
        self.scrollStatus.pack(side=Tkinter.RIGHT, fill=Tkinter.Y)
        self.textStatus.pack()

  def fail(self):
    tkMessageBox.showerror("Connection Failed...", "Great job, Eran! I can't talk to the server.\nPlease make sure it is running, and you entered the right port.\n\n\n(If this isn't a helpful error message, I don't know what is...)")
    self.view('connect') # See? Told you it was a nifty trick.

Interface() #Liftoff!