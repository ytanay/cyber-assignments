Assignment #1
=============
Remote command execution over TCP sockets. Supports multiple clients. Threaded. Has a GUI. Also, it's a bit passive-aggressive.

Yes, it's that good.

*Written by Yotam Tanay, 06-08/09/14, for Eran Bineth.*


**Important!**
Certain system commands, like `netstat` and `tracert`, take a long time to run (and sometimes never finish). The server will wait for them to exit before buffering data to the client. This will make the client become unresponsive. 
In theory, this can be fixed by streaming `sterr` of the process over the socket, but I was lead to believe this is outside the scope of this project.

##Notes##


###Server###
  
- To run the server, execute `python server.py`
- You may specify a port for the server to bind on using `python servery.py --port 1337`. The default is 8888
- Specify the command list using the `commands.json` file. It's... a JSON file.

###Client###

- To use the GUI interface, run `python interface.py`, NOT `python client.py`.
- Seriously, don't do that. It'll do nothing
- If you want to use a CLI interface, you should have asked for it in the project spec.