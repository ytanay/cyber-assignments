'''
server.py
ytanay/cyber-assignments/assignment1
Part of a series in... 'cyber'

As the name implies, this is the server.

Written by Yotam Tanay, 2014
'''

import socket, sys, uuid, subprocess, os, argparse, re, textwrap, thread #Funny story about the threading module - they totally messed it up in Py3.

class Console:
  '''
    I'd just like to state, again, my utter hatred for Windows, and specifically Python on Win32.
    Seriously, Windows. You're still using the same command interpreter you had in IBM PC-DOS. Do you know when that was released?

    1981.

    It's just shameful.
  
    What you see below is the beautiful coloring system I use all the time on Linux.

    INFO = '\033[94m' # blue info
    SUCCESS = '\033[92m' # green success
    WARNING = '\033[93m' # yellow warning
    FAIL = '\033[91m' # red failure
    RESET = '\033[0m' # reset!
    BOLD = '\033[1m' # bold text
    ITALICS = '\033[3m' #italic text

    And what you see here is... the Windows equivalent
  
  '''

  INFO = ''
  SUCCESS = ''
  WARNING = ''
  FAIL = ''
  RESET = ''
  BOLD = ''
  ITALICS = ''

  @staticmethod
  def write(string):
    '''Write stuff to stdout, without a newline'''
    sys.stdout.write(string)

  @staticmethod
  def done(message=''):
    '''Appends a success message'''
    sys.stdout.write(message + '%sDONE!%s\n' % (Console.SUCCESS, Console.RESET))

  @staticmethod
  def error(message=''):
    '''Appends an error message'''
    sys.stdout.write('%s%s%s' % (Console.FAIL, message or 'ERROR! ', Console.RESET))

class Server(object):

  COMMANDS = []

  '''This is the server. It does stuff'''
  def __init__(self):
    super(Server, self).__init__()
    
    self.parse() # parses command line args
    self.init() # inits the server
    self.accept() # waits for clients
    self.socket.close() # closes the socket

  def parse(self):
    '''Parses command line arguments'''

    parser = argparse.ArgumentParser(description='Server service for assignment 1')
    parser.add_argument('--port', type=int, help='Port to run service on')
    parser.add_argument('--commands', type=str, help='Comma delimited file with server commands', default='commands')
    parser.add_argument('--unsafe', type=bool, help='Allows arbitrary command execution', default=False)
    self.args = parser.parse_args()

  def init(self):
    '''Creates the server. This is of particular importance.'''

    print '\n%sserver.py%s - ytanay/cyber-assignments/assignment1 - Written by Yotam Tanay, 2014\n' % (Console.ITALICS+Console.BOLD, Console.RESET)
    print textwrap.fill('DISCLAIMER: I\'m an interactive server. My creator would like to state that this is considered to be a %sVERY BAD THING%s and he feels strongly that this should never be done. On an unrelated note, he has been fired.' % (Console.FAIL, Console.RESET), 80)
    
    Console.write('\nCreating a standard TCP/IP socket... ')
    self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    Console.done()

    Console.write('\nBinding socket...\n')
    if isinstance(self.args.port, int):
      print '\tIt looks like you\'ve specified the port number %d using --port' % self.args.port
      self.port = self.args.port
    else:
      Console.write('\tYou haven\'t specified a port number. Please give it to me now (Perhaps 8888?): ')
      self.port = self.Interactors.port()

    while True:
      try:
        self.socket.bind(('', self.port))
        break
      except socket.error as error:
        Console.write('\t%sI failed to bind the socket.%s\n\tThe port is probably in use, or you do not have permission to use it.\n\tPlease enter another port: ' % (Console.FAIL, Console.RESET))
        self.port = self.Interactors.port()

    Console.done('\tFinalizing... ')

    Console.done('\nRegistering socket listener... ')
    self.socket.listen(10)

    Console.write('\nPreparing commands from %s... ' % self.args.commands)
    Server.COMMANDS = re.sub(r'(\n|  )', '', open(self.args.commands, 'r').read()) # reads the list of commands and turns them into an array.
    Console.done()

    print '\nServer ready for connections!\n'
    print 'You may connect as many clients as you\'d like on port %d.' % self.port
    print 'To shut down the server use CTRL+C, or close this terminal window (better!)'
    print 'Make sure not to keep any clients running when you close the server.\n'

  def accept(self):
    '''This method waits forever until clients try to connect. When they do, it creates a new thread, and goes back to waiting forever.'''

    while True:
      connection, address = self.socket.accept() # Wait to get a new connection. This is a blocking call, obviously.
      print 'Server::accept(): accepted connection from ' + address[0] + ':' + str(address[1])
      
      '''
        Turns out the standard forking mechanism is quite unreliable on Windows. Surprise!
        Oh wait. Not that surpising. Hmm...
      '''
      thread.start_new_thread(self.connect, (connection, Server.COMMANDS)) 

  @staticmethod
  def connect(conn, commands):
    '''
    This fires when a new client connects.
    Here is my simple protocol for this assignemt:
    Currently, the only requests the client can send are 'INFO' and 'EXEC {{command}}'.
      INFO - updates the command list and other server info
      EXEC {{command}} - attempts to execute the command on the server and buffers the output to the client
    The responses from the server follow a similar form:
      HELLO {{server_version}} - connection accepted
      IDENTITY {{id}} - sets the seesion identifier, currently unused
      STATUS {{current_status}} - allows for an authentication system in the future
      TEXT {{...}} - comment for the client to show for the user
      WRITE {{..}} - buffers a variable amount of data from a command execution; implies the command is still being executed
      DONE - implies that the command execution is finihsed and the client can safely buffer the response
      ERROR {{error}} - reports an error from the command execution or another failure (network, stream, whatever).
    '''

    # Why do I generate a UUID for new connections? Great question! I thought of implementing a session system, but then decided it was overkill.
    # I still send it though, because future thinking, that's why.
    conn.send('HELLO server/0.2.5\nIDENTITY %s\nSTATUS connected\nTEXT Hello, you\'re now connected to the server!\nTEXT Written by Yotam Tanay for a C.S. course in \'Cyber\'.\n' % uuid.uuid4())
    conn.send('UPDATE_COMMANDS -> %s' % commands)

    # Keeps us in the loop... get it?
    while True:
      data = conn.recv(1024) # Pick up some data from the client
      if not data: 
          break # This happens when the client disconnects.

      if data.startswith('EXEC'):
          command = data[5:] # Trims the header
          print 'Server:exec(): will execute command "%s"' % command
          data = os.popen(command)
          for line in data:
            conn.sendall('WRITE ' + line) # Signals that there is still output to be buffered
          reply = '\nDONE' # Signals that all output has been buffered

      if not reply:
          reply = 'ERROR command execution failed!'
      
      conn.sendall(reply)

    conn.close() #We're out, so let's clean up.

  class Interactors:
    '''A simple wrapper for any repeating interactive errors'''

    @staticmethod
    def port():
      while True:
        try:
          port = int(input())
          if port < 1 or port > 65535:
            Console.write('\tThat number is invalid (the range is 1-65535). Enter a valid port: ')
          else:
            return port
        except Exception, e:
          Console.error('\tOops... ')
          Console.write('Ports are numbers, not anything else. Try again: ')
        
server = Server() # Liftoff!
